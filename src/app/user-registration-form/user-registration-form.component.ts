import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

export function ValidateConfirmPassword(control: AbstractControl) {

  const password = control.root.get('password').value;
  const confirmPassword = control.root.get('confirmPassword').value;

  if (password && password.length && password !== confirmPassword) {
    control.root.get('confirmPassword').setErrors({ passwordMismatch: true})
    return { passwordMismatch: true };
  }
  
  return null;

}


@Component({
  selector: 'app-user-registration-form',
  templateUrl: './user-registration-form.component.html',
  styleUrls: ['./user-registration-form.component.css']
})
export class UserRegistrationFormComponent implements OnInit {

  registrationForm: any;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.registrationForm = new FormGroup({
      username: new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]+$'), Validators.minLength(5), Validators.maxLength(15)])),
      password: new FormControl('', Validators.compose([Validators.required])),
      confirmPassword: new FormControl('', Validators.compose([Validators.required])),
      age: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]*'), Validators.min(18), Validators. max(118)]))
      },{
        updateOn: 'submit',
        validators: ValidateConfirmPassword
      });

  }

  onSubmit() {
    this.validateAllFormFields(this.registrationForm);
  }

  reset() {
    this.registrationForm.reset();
  }

  validateAllFormFields(formGroup: FormGroup) {
    formGroup.markAsTouched({ onlySelf: false })
    
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return !this.registrationForm.get(field).valid && this.registrationForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-success': !this.isFieldValid(field) && this.registrationForm.get(field).touched,
      'has-feedback': this.isFieldValid(field),
    };
  }

}
