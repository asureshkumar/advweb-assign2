# Assign2

This project uses Angular framework as javascript framework and bootstrap as css framework. 

Assignment 2 has been deployed at [http://suresa5-advweb-assign2.surge.sh/](http://suresa5-advweb-assign2.surge.sh/)


## Development server

Run `npm install && ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

